FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8-slim

# TimeZone settings
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Install dependencies
COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . /app
