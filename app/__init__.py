import logging

from fastapi import Body, FastAPI

from .services.telegram import send_message

logging.basicConfig(
    level=logging.INFO,
)

app = FastAPI()


@app.post("/telegram")
async def receive_telegram_alert(body: dict = Body(...)):
    text = body.get("_message")
    await send_message(text)
    return {"Success": True}
