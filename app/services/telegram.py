import logging
from os import getenv

from aiogram import Bot
from aiogram.types import ParseMode
from aiogram.utils.exceptions import TelegramAPIError

logger = logging.getLogger(__name__)

token = getenv("TELEGRAM_TOKEN", None)
if not token:
    raise RuntimeError("`TELEGRAM_TOKEN` env variable is empty.")

recipient = getenv("TELEGRAM_RECIPIENT", None)
if not recipient:
    raise RuntimeError("`TELEGRAM_RECIPIENT` env variable is empty.")

bot = Bot(token=token, parse_mode=ParseMode.HTML)


async def send_message(text: str):
    try:
        await bot.send_message(
            chat_id=recipient,
            text=text,
        )
    except TelegramAPIError as e:
        logger.error(f"{type(e).__name__}: {e}")
