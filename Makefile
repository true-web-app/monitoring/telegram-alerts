isort:
	isort . --profile black

black:
	black .

flake8:
	flake8 .

lint: isort black flake8

requirements:
	poetry export -E ultra --without-hashes -f requirements.txt -o requirements.txt
	poetry export -E ultra --without-hashes -f requirements.txt -o requirements_dev.txt --dev
