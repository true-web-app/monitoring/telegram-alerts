# Telegram Alerts

Influx + Telegraf + Telegram Alerts

### Notification Endpoint
```
POST http://telegram-alerts/telegram
```

### Message example
```
<b>#${ r.host }</b>
${ r._check_name }: <code>${ r._level }</code>
```

### docker-compose.yml example
```yaml
version: '3'
services:

  influxdb:
    image: influxdb:2.0
    restart: unless-stopped
    volumes:
      - "influxdbv2:/var/lib/influxdb2"
    ports:
      - 127.0.0.1:8086:8086
    networks:
      - influxdb-telegraf-net

  telegraf:
    image: telegraf
    restart: unless-stopped
    hostname: MyHost
    volumes:
      - "./telegraf.conf:/etc/telegraf/telegraf.conf:ro"
      - "/:/hostfs:ro"
      - "/var/run/docker.sock:/var/run/docker.sock"
    environment:
      - HOST_ETC=/hostfs/etc
      - HOST_PROC=/hostfs/proc
      - HOST_SYS=/hostfs/sys
      - HOST_VAR=/hostfs/var
      - HOST_RUN=/hostfs/run
      - HOST_MOUNT_PREFIX=/hostfs
      - INFLUX_TOKEN=my_influx_token
    links:
      - influxdb
    networks:
      - influxdb-telegraf-net

  telegram-alerts:
    image: registry.gitlab.com/true-web-app/monitoring/telegram-alerts:latest
    restart: unless-stopped
    links:
      - influxdb
    networks:
      - influxdb-telegraf-net
    environment:
      APP_MODULE: app:app
      TELEGRAM_TOKEN: 1234567890:qwertyuiop
      TELEGRAM_RECIPIENT: -1234567890


volumes:
  influxdbv2:
    driver: local

networks:
  influxdb-telegraf-net:
    driver: bridge

```
